package connectionpool;

import dev.langchain4j.model.chat.ChatLanguageModel;
import dev.langchain4j.model.openai.OpenAiChatModel;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import java.time.Duration;

public class ChatLanguageModelFactory extends BasePooledObjectFactory<ChatLanguageModel> {
    private final String apiKey;

    public ChatLanguageModelFactory(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public ChatLanguageModel create() throws Exception {
        return OpenAiChatModel.builder()
                .apiKey(apiKey)
                .modelName("gpt-4o")
                .maxTokens(300)
                .logRequests(true)
                .logResponses(true)
                .timeout(Duration.ofSeconds(60))
                .build();
    }

    @Override
    public PooledObject<ChatLanguageModel> wrap(ChatLanguageModel model) {
        return new DefaultPooledObject<>(model);
    }
}

