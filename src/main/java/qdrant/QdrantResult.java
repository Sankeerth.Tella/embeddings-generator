package qdrant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QdrantResult {
    private long id;
    private String url;
    private float score;
    private long requests;
    private int batch; // Only included for spaced search results
}

