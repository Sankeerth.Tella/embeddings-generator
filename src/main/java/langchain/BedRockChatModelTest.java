package langchain;

import dev.langchain4j.model.bedrock.BedrockAnthropicMessageChatModel;
import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.regions.Region;


public class BedRockChatModelTest {
    public static void main(String[] args) {
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(
                AwsBasicCredentials.create("AWS_ACCESS_KEY_ID", "AWS_SECRET_ACCESS_KEY")
        );
        BedrockAnthropicMessageChatModel model = BedrockAnthropicMessageChatModel
                .builder()
                .maxTokens(300)
                .region(Region.US_EAST_1)
                .model(BedrockAnthropicMessageChatModel.Types.AnthropicClaude3SonnetV1.toString())
                .model("anthropic.claude-3-sonnet-20240229-v1:0")
                .maxRetries(1)
                .credentialsProvider(credentialsProvider)
                .build();
        String joke = model.generate("Tell me a joke about Java");
        System.out.println(joke);
    }
}