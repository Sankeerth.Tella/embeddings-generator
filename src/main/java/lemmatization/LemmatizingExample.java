package lemmatization;

import edu.stanford.nlp.simple.Sentence;
import java.util.*;

public class LemmatizingExample {
    public static String text = "Hello World!!";

    public static void main(String[] args) {
        List<String> result = new Sentence(text).lemmas();
        System.out.println(result.toString());

        //
//        Properties props = new Properties();
//        props.put("annotators", "tokenize, pos, lemma");
//        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
//
//        CoreDocument cd = new CoreDocument(text);
//        pipeline.annotate(cd);
//        List<String> lemmas=  cd.tokens().stream()
//                .map(cl -> cl.lemma())
//                .collect(Collectors.toList());
//        System.out.println(lemmas);
    }
}
