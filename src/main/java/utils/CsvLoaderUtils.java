package utils;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class CsvLoaderUtils {
    public static CsvData LoadData(String csvFilePath){
        List<String> sentences = new ArrayList<>();
        List<String> keys = new ArrayList<>();
            List<String[]> rows;
            try (CSVReader reader = new CSVReader(new FileReader(csvFilePath))) {
                reader.readNext();
                rows = reader.readAll();
                for (String[] row : rows) {
                    keys.add(row[0]);
                    sentences.add(row[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CsvException e) {
                throw new RuntimeException(e);
            }
        return new CsvData(keys, sentences);
    }

    public static void writeEmbeddingsToCSV(List<double[]> embeddings, String csvFilePath, List<String> keys) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < embeddings.size(); i++) {

            String embeddingString = Arrays.toString(embeddings.get(i));
            String key = keys.get(i);
            sb.append(key).append(",");

            sb.append("\"").append(embeddingString).append("\"");
            sb.append("\n");
        }
        try (FileWriter writer = new FileWriter(csvFilePath)) {
            writer.write(sb.toString());
        }
    }
}
