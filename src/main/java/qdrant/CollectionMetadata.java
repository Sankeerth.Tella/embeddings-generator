package qdrant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CollectionMetadata {
    private String collectionName;
    private long universeSampleSize;
    private long universeSampleRequests;
    private long universeActiveSize;
    private long universeActiveRequests;
    private long creationTimeMs;
    private String creationUser;
}

