package langchain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.langchain4j.model.chat.ChatLanguageModel;
import dev.langchain4j.model.input.Prompt;
import dev.langchain4j.model.input.structured.StructuredPrompt;
import dev.langchain4j.model.input.structured.StructuredPromptProcessor;
import dev.langchain4j.model.openai.OpenAiChatModel;

import java.io.IOException;
import java.time.Duration;
import java.util.List;

public class OpenAiChatModelTest {
    @StructuredPrompt(
            "You are an application whose job is to read an advertiser's description of web content and then rate " +
                    "how confident you are that the description of web content is contextually relevant to each of a set of web " +
                    "content categories for the purposes of advertising on that content. The different web content categories to " +
                    "consider are: {{catalogCategories}}.\n Confidence level is between 0 and 10. Only output results for categories " +
                    "which you rate higher than 3 Your response should be formatted using the following as an example:" +
                    "[{\"explanation\": \"short explanation for your rating\", \"contextCategory\": \"the name of the category\", \"confidenceScore\": (integer between 0 and 10)}]" +
                    "Do not include anything else in your response. The web content description is: {{text}}"
    )
//    @StructuredPrompt(
//            "You are an application whose job is to read an advertiser's description of web content and then rate " +
//                    "how confident you are that the description of web content is contextually relevant to each of a set of web " +
//                    "content categories for the purposes of advertising on that content. The different web content categories to " +
//                    "consider are: {{catalogCategories}}.\n Confidence level is between 0 and 10. Only output results for categories " +
//                    "which you rate higher than 3. Your response must strictly be a valid JSON array of objects. Do not include any explanations, text, or additional information**.\n" +
//                    "[{\"explanation\": \"short explanation for your rating\", \"contextCategory\": \"the name of the category\", \"confidenceScore\": (integer between 0 and 10)}]" +
//                    "Strictly output a valid JSON array without any additional characters. The web content description is: {{text}}"
//    )
    static class CategorySuggestionPrompt {
        private String text;
        private String catalogCategories;
    }
    public static void main(String[] args) {
        CategorySuggestionPrompt categorySuggestionPrompt = new CategorySuggestionPrompt();
        categorySuggestionPrompt.text="Debit & Checking Services, Checking account, Free checking account, Best checking account";
        categorySuggestionPrompt.catalogCategories = "Arts & Entertainment. Autos & Vehicles. Beauty & Cosmetology. Books & Literature. Business. Careers, Jobs, & Education. Computers & Electronics. Content Channel. Content Media Format. Content Source. Content Type. Economy. Finance. Food & Drink. Hobbies, Interests, Leisure & Games. Home & Garden. Industry. Internet & Telecom. Law, Government, News & Politics. Medical Health. Online Communities. People & Society. Personal Care. Personal Finance. Pets & Animals. Real Estate. Reference. Science. Shopping. Sports. Style & Fashion. Travel & Transportation. Consumer Advocacy & Protection. Coupons & Discount Offers. Customer Services. Identity Theft Protection. Product Reviews & Price Comparisons";
        Prompt prompt = StructuredPromptProcessor.toPrompt(categorySuggestionPrompt);
        ChatLanguageModel model = OpenAiChatModel.builder()
                .apiKey("sk-svcacct-GkcKj3AKL40P-DjaJ7RrWAMmC7WQjw1BnUFWG5RdcgwGm2vcwFmqg7_VDzR1M1Y4TZWZlpT3BlbkFJmqU2YlMqKQbyUTicpKa--UZv1cqRUCxxNmxIspwM4UFGvNJFw-nn7lgtA6uULunon_ZvYA")
                .modelName("gpt-4o-2024-08-06")
                .maxTokens(300)
                .logRequests(false)
                .logResponses(false)
                .timeout(Duration.ofSeconds(60))
                .responseFormat("json_schema")
                .strictJsonSchema(true)
                .build();
        String response = model.generate(prompt.toUserMessage()).content().text();
        ObjectMapper mapper = new ObjectMapper();

        try {
            List<CategoryInfo> categoryInfoList = mapper.readValue(response, new TypeReference<List<CategoryInfo>>() {});
            for (CategoryInfo info : categoryInfoList) {
                System.out.println(info);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}