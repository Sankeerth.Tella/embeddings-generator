package langchain;

import dev.langchain4j.model.anthropic.AnthropicChatModel;
import dev.langchain4j.model.chat.ChatLanguageModel;
import dev.langchain4j.model.input.Prompt;
import dev.langchain4j.model.input.structured.StructuredPrompt;
import dev.langchain4j.model.input.structured.StructuredPromptProcessor;

import java.time.Duration;
import java.util.List;

import static java.util.Arrays.asList;

public class AnthropicChatModelTest {
    @StructuredPrompt("Create a recipe with {{size}} words of a {{dish}} that can be prepared using only {{ingredients}}")
    static class CreateRecipePrompt {
        private int size;
        private String dish;
        private List<String> ingredients;
    }
    public static void main(String[] args) {
        AnthropicChatModelTest.CreateRecipePrompt createRecipePrompt = new AnthropicChatModelTest.CreateRecipePrompt();
        createRecipePrompt.size=200;
        createRecipePrompt.dish = "salad";
        createRecipePrompt.ingredients = asList("cucumber", "tomato", "feta", "onion", "olives");
        Prompt prompt = StructuredPromptProcessor.toPrompt(createRecipePrompt);
        ChatLanguageModel model = AnthropicChatModel.builder()
                .apiKey("ANTHROPIC_KEY")
                .modelName("claude-3-haiku-20240307")
                .maxTokens(300)
                .logRequests(true)
                .logResponses(true)
                .timeout(Duration.ofSeconds(5))
                .build();

        String joke = model.generate(prompt.toUserMessage()).content().text();
        System.out.println(joke);
    }
}
