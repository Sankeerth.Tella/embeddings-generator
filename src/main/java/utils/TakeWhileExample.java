package utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TakeWhileExample {
    public static void main(String[] args) {
        // Sample list of words
        List<String> words = List.of("Hello", "world", "this", "is", "a", "test", "of", "takeWhile", "and", "forEach", "in", "Java");

        // StringBuilder to accumulate the result
        StringBuilder sentenceBuilder = new StringBuilder();

        // Concatenating words while ensuring the total length does not exceed 30 characters
        Stream.concat(words.stream(), Stream.of("!")) // Adding a punctuation mark to end the sentence
                .takeWhile(word -> {
                    // This condition is evaluated before appending each word
                    boolean canAdd = sentenceBuilder.length() + word.length() <= 30;
                    System.out.println("Evaluating word: " + word + ", Can add: " + canAdd);
                    return canAdd; // The result of this evaluation determines if the word is included
                }).forEach(word -> {
                    System.out.println("test");
                    if (sentenceBuilder.length() > 0) {
                        sentenceBuilder.append(" "); // Add a space before the next word if not the first word
                    }
                    sentenceBuilder.append(word); // Append the word to the sentence
                });

        // Output the final sentence
        System.out.println("Final sentence: " + sentenceBuilder.toString());
    }
}

