package langchain;

import dev.langchain4j.model.chat.ChatLanguageModel;
import dev.langchain4j.model.input.structured.StructuredPrompt;
import dev.langchain4j.model.openai.OpenAiChatModel;
import dev.langchain4j.model.output.structured.Description;
import dev.langchain4j.service.AiServices;

import java.time.Duration;
import java.util.List;

public class OpenAIAssistant {

    static class Suggestion {

        @Description("short explanation for your rating, 1 sentence maximum")
        private String explanation;

        @Description("the name of the category")
        private String contextCategory;

        @Description("integer between 0 and 10")
        private Integer confidenceScore;
    }

    static class CategorySuggestion {

        @Description("list of content category suggestions with confidence score")
        private List<Suggestion> suggestions;
    }


    static class GeneratedText {
        @Description("must contain only body of the article.")
        private String articleBody;
    }

    @StructuredPrompt("You are an application whose job is to read an advertiser's description of web content and then rate \" +\n" +
            "                    \"how confident you are that the description of web content is contextually relevant to each of a set of web \" +\n" +
            "                    \"content categories for the purposes of advertising on that content. The different web content categories to \" +\n" +
            "                    \"consider are: {{catalogCategories}}.\\n Confidence level is between 0 and 10. Only output results for categories \" +\n" +
            "                    \"which you rate higher than 3. The web content description is: {{text}}")
    static class CategorySuggestionPrompt {
        private String text;
        private String catalogCategories;
    }

    @StructuredPrompt(
            "Write a {{numberOfWords}} word article that looks like the content an advertiser would want to find " +
                    "on the internet for the given keywords: {{description}}."
    )
    static class GeneratedTextPrompt {
        private Integer numberOfWords;
        private String description;
    }

    interface EstimationAssistant {
        CategorySuggestion getCategorySuggestions(CategorySuggestionPrompt prompt);
        GeneratedText getGeneratedText(GeneratedTextPrompt prompt);
    }

    public static void main(String[] args) {
        ChatLanguageModel model = OpenAiChatModel.builder()
                .apiKey("OPEN_AI_KEY")
                .modelName("gpt-4o-2024-08-06")
                .maxTokens(3000)
                .logRequests(false)
                .logResponses(false)
                .timeout(Duration.ofSeconds(60))
                .responseFormat("json_schema")
                .strictJsonSchema(true)
                .temperature(0D)
                .build();
        EstimationAssistant assistant = AiServices
                .builder(EstimationAssistant.class)
                .chatLanguageModel(model)
                .build();

        CategorySuggestionPrompt prompt = new CategorySuggestionPrompt();
        prompt.text="Debit & Checking Services, Checking account, Free checking account, Best checking account";
        prompt.catalogCategories = "Arts & Entertainment. Autos & Vehicles. Beauty & Cosmetology. Books & Literature. Business. Careers, Jobs, & Education. Computers & Electronics. Content Channel. Content Media Format. Content Source. Content Type. Economy. Finance. Food & Drink. Hobbies, Interests, Leisure & Games. Home & Garden. Industry. Internet & Telecom. Law, Government, News & Politics. Medical Health. Online Communities. People & Society. Personal Care. Personal Finance. Pets & Animals. Real Estate. Reference. Science. Shopping. Sports. Style & Fashion. Travel & Transportation. Consumer Advocacy & Protection. Coupons & Discount Offers. Customer Services. Identity Theft Protection. Product Reviews & Price Comparisons";
        CategorySuggestion categorySuggestions = assistant.getCategorySuggestions(prompt);
        categorySuggestions.suggestions.forEach( suggestion -> {
            System.out.println(suggestion.confidenceScore);
            System.out.println(suggestion.contextCategory);
            System.out.println(suggestion.explanation);
        });

        GeneratedTextPrompt textPrompt = new GeneratedTextPrompt();
        textPrompt.numberOfWords=250;
        textPrompt.description = "Debit & Checking Services, Checking account, Free checking account, Best checking account";
        GeneratedText generatedText = assistant.getGeneratedText(textPrompt);
        System.out.println(generatedText.articleBody);

    }
}