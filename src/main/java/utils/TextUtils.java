package utils;

import edu.stanford.nlp.simple.Sentence;
import java.util.*;
import java.util.stream.Collectors;

public final class TextUtils {

    private static final Set<String> STOPWORDS = new HashSet<>(Arrays.asList(
            "i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
            "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself",
            "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these",
            "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do",
            "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while",
            "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before",
            "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again",
            "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each",
            "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than",
            "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"
    ));

    public static String prepareDocument(String document) {
        String textAlpha = document.toLowerCase().replaceAll("[^a-z]+", " ");
        return getTextEdges(textAlpha);
    }

    public static String prepareSentence(String document) {
        String textNonStop = removeStopwords(document);
        String textNonSmall = removeSmallTokens(textNonStop);
        return lemmatize(textNonSmall);
    }

    public static String removeStopwords(String text) {
        return Arrays.stream(text.split("\\s+"))
                .filter(word -> !STOPWORDS.contains(word))
                .collect(Collectors.joining(" "));
    }

    public static String removeSmallTokens(String text) {
        return Arrays.stream(text.split("\\s+"))
                .filter(word -> word.length() >= 3)
                .collect(Collectors.joining(" "));
    }

    public static String getTextEdges(String text) {
        int headCount = 128;
        int tailCount = 384;
        String[] textParts = text.split("\\s+");

        if (textParts.length > headCount + tailCount) {
            return String.join(" ", Arrays.copyOfRange(textParts, 0, headCount)) + " " +
                    String.join(" ", Arrays.copyOfRange(textParts, textParts.length - tailCount, textParts.length));
        } else {
            return text;
        }
    }

    public static String lemmatize(String text){
        List<String> lemmas = new Sentence(text).lemmas();
        return String.join(" ", lemmas);
    }

    public static List<String> splitDocument(String document, int splitLength) {
        List<String> result = new ArrayList<>();
        List<String> currentSentence = new ArrayList<>();
        int currentLength = 0;

        for (String word : document.split("\\s+")) {
            if (currentLength + word.length() > splitLength) {
                result.add(String.join(" ", currentSentence));
                currentSentence = new ArrayList<>();
                currentSentence.add(word);
                currentLength = word.length() + 1;
            } else {
                currentSentence.add(word);
                currentLength += word.length() + 1;
            }
        }

        if (!currentSentence.isEmpty()) {
            result.add(String.join(" ", currentSentence));
        }

        return result;
    }
}

