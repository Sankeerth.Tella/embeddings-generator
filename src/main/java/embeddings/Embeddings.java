package embeddings;

import utils.CsvData;
import java.io.IOException;
import java.util.*;
import static utils.CsvLoaderUtils.*;
import static utils.EmbeddingUtils.calculateMean;
import static utils.EmbeddingUtils.getEmbedding;
import static utils.TextUtils.*;

public class Embeddings {
    public static void main(String[] args) throws Exception {
        String inputPath = "/Users/stella/Desktop/embeddings-generator/src/main/resources/generated_text_prod_sample_100_head_and_tail_v2.csv";
//        String outputFileName = "generated_text_prod_sample_100_head_and_tail_v2_java_embeddings_double.csv";
                String outputFileName = "test2.csv";

        boolean applyCleaning = false;

        CsvData data = LoadData(inputPath);

        List<String> documents = data.getDocuments().subList(0,5);
        List<String> keys = data.getKeys();

        List<double[]> allEmbeddings = new ArrayList<>();

        for (String document : documents) {
            if(applyCleaning){
                String cleanedDocument = prepareDocument(document);
                List<String> splitDoc = splitDocument(cleanedDocument, 512);
                List<double[]> tempEmbeddings = new ArrayList<>();
                for(String doc: splitDoc){
                    String cleanedSentence = prepareSentence(doc);
                    tempEmbeddings.add(getEmbedding(cleanedSentence));
                }
                allEmbeddings.add(calculateMean(tempEmbeddings));
            }else{
                allEmbeddings.add(getEmbedding(document));
            }

        }

        try {
            writeEmbeddingsToCSV(allEmbeddings, outputFileName, keys);
            System.out.println("CSV file created successfully!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
