package utils;

import ai.djl.huggingface.tokenizers.Encoding;
import ai.djl.huggingface.tokenizers.HuggingFaceTokenizer;
import ai.onnxruntime.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class EmbeddingUtils {

    private static Map<String, String> tokenizerOptions = new HashMap<>();
    private static HuggingFaceTokenizer tokenizer;
    private static OrtEnvironment environment;
    private static OrtSession session;

    static {
        tokenizerOptions.put("maxLength", "512");
        tokenizerOptions.put("truncation", "true");
        try {
            tokenizer = HuggingFaceTokenizer.newInstance(Paths.get("/Users/stella/Desktop/embeddings-generator/src/main/resources/tokenizer.json"), tokenizerOptions);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            environment = OrtEnvironment.getEnvironment();
            session = environment.createSession("/Users/stella/Desktop/embeddings-generator/src/main/resources/bert-base-uncased_latest.onnx");
        } catch (OrtException e) {
            throw new RuntimeException(e);
        }
    }

    public static double[] getEmbedding(String sentence) throws OrtException {
        Encoding encoding = tokenizer.encode(sentence);

        long[] inputIds = encoding.getIds();
        long[] attentionMask = encoding.getAttentionMask();
        long[] tokenTypeIds = encoding.getTypeIds();
        long[] specialTokensMask = encoding.getSpecialTokenMask();
        double[] embeddingsVector;
        // Prepare tensors
        OnnxTensor inputIdsTensor = OnnxTensor.createTensor(environment, new long[][]{inputIds});
        OnnxTensor attentionMaskTensor = OnnxTensor.createTensor(environment, new long[][]{attentionMask});
        OnnxTensor tokenTypeIdsTensor = OnnxTensor.createTensor(environment, new long[][]{tokenTypeIds});
        Map<String, OnnxTensor> inputs = new HashMap<>();
        inputs.put("input_ids", inputIdsTensor);
        inputs.put("attention_mask", attentionMaskTensor);
        inputs.put("token_type_ids", tokenTypeIdsTensor);
        try (OrtSession.Result results = session.run(inputs)) {
            OnnxValue lastHiddenState = results.get(0);
            float[][][] tokenEmbeddings = (float[][][]) lastHiddenState.getValue();
            embeddingsVector = meanPooling(tokenEmbeddings[0], attentionMask, specialTokensMask);

        } catch (OrtException e) {
            throw new RuntimeException(e);
        }
        return embeddingsVector;
    }

    public static double[] meanPooling(float[][] tokenEmbeddings, long[] attentionMask, long[] specialTokensMask) {
        int numDimensions = tokenEmbeddings[0].length;
        double[] mean = new double[numDimensions];
        int denominator = 0;
            for (int j = 0; j < tokenEmbeddings.length; j++) { // for each token
                // we need to make sure we ignore all added tokens like [CLS], [SEP], [PAD]
                if (attentionMask[j] == 1 && specialTokensMask[j] == 0) {
                    denominator++;
                    for (int k = 0; k < numDimensions; k++) { // for each dimension
                        mean[k] += tokenEmbeddings[j][k];
                    }
                }
            }
            for (int k = 0; k < numDimensions; k++) { // for each dimension
                mean[k] /= denominator;
            }
        return mean;
    }

    public static double[] calculateMean(List<double[]> wordEmbeddings) {
        if (wordEmbeddings == null || wordEmbeddings.isEmpty()) {
            throw new IllegalArgumentException("The list of word embeddings cannot be null or empty");
        }

        int vectorSize = wordEmbeddings.get(0).length;
        double[] meanVector = new double[vectorSize];
        int numVectors = wordEmbeddings.size();

        // Sum each dimension using double for higher precision
        for (double[] vector : wordEmbeddings) {
            for (int i = 0; i < vectorSize; i++) {
                meanVector[i] += vector[i];
            }
        }

        // Calculate the mean for each dimension
        for (int i = 0; i < vectorSize; i++) {
            meanVector[i] /= numVectors;
        }

        return meanVector;
    }

}
