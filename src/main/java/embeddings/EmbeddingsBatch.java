package embeddings;

import java.io.FileReader;
import java.nio.file.Paths;
import java.util.*;

import ai.djl.huggingface.tokenizers.Encoding;
import ai.djl.huggingface.tokenizers.HuggingFaceTokenizer;
import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OnnxValue;
import ai.onnxruntime.OrtEnvironment;
import ai.onnxruntime.OrtSession;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EmbeddingsBatch {
    public static void main(String[] args) throws Exception {



        boolean useCSV = false;
        List<String> sentences = new ArrayList<>();
        List<String> urls = new ArrayList<>();
        String csvFilePath = "/Users/stella/Desktop/embeddings-generator/src/main/resources/embeddings_sample_text_1k.csv";
        if (useCSV) {
            List<String[]> rows;
            try (CSVReader reader = new CSVReader(new FileReader(csvFilePath))) {
                reader.readNext();
                rows = reader.readAll();
                for (String[] row : rows) {
                    sentences.add(row[1]);
                    urls.add(row[0]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CsvException e) {
                throw new RuntimeException(e);
            }

        } else {
            sentences = Arrays.asList("In a small village nestled in the mountains, there lived a wise old man who knew many secrets of the world.",
                    "The bustling city streets were filled with the sounds of cars honking, people talking, and street vendors selling their goods.",
                    "As the sun set over the horizon, the sky turned a brilliant shade of orange and pink, casting long shadows across the landscape.",
                    "In the middle of the dense forest, a hidden waterfall cascaded down into a crystal-clear pool, surrounded by lush greenery.",
                    "The ancient castle, with its tall towers and thick stone walls, stood as a testament to the battles fought and victories won over centuries.",
                    "At the edge of the serene lake, children played and laughed while the gentle breeze rustled the leaves of the nearby trees.",
                    "During the long voyage across the ocean, the sailors encountered fierce storms, magnificent sunsets, and a pod of playful dolphins.",
                    "In the crowded marketplace, merchants from distant lands bartered and traded exotic spices, colorful fabrics, and intricate jewelry.",
                    "Under the starry night sky, the astronomer peered through his telescope, marveling at the vastness and beauty of the universe.",
                    "With determination in their hearts, the team of explorers ventured into the uncharted wilderness, eager to uncover its hidden treasures.");
        }

//        sentences = sentences.subList(0, 100);

        long tokenizerStartTime = System.currentTimeMillis();
        Map<String, String> tokenizerOptions = new HashMap<>();
        tokenizerOptions.put("maxLength", "512");
        tokenizerOptions.put("padding", "true");
        tokenizerOptions.put("truncation", "true");
        HuggingFaceTokenizer tokenizer = HuggingFaceTokenizer.newInstance(Paths.get("/Users/stella/Desktop/embeddings-generator/src/main/resources/tokenizer.json"), tokenizerOptions);
        long tokenizerEndTime = System.currentTimeMillis();

        long modelLoadStartTime = System.currentTimeMillis();
        OrtEnvironment environment = OrtEnvironment.getEnvironment();
        OrtSession session = environment.createSession("/Users/stella/Desktop/embeddings-generator/src/main/resources/model_transformers.onnx");
        long modelLoadEndTime = System.currentTimeMillis();

        long embeddingsStartTime = System.currentTimeMillis();
        long tokenGenerationStartTime = System.currentTimeMillis();

        Encoding[] encodings = tokenizer.batchEncode(sentences);
        long tokenGenerationEndTime = System.currentTimeMillis();
        long[][] inputIds = new long[encodings.length][];
        long[][] attentionMaskIds = new long[encodings.length][];
        long[][] tokenTypeIds = new long[encodings.length][];
        long[][] specialTokenMask = new long[encodings.length][];


        for (int i = 0; i < encodings.length; i++) {
            inputIds[i] = encodings[i].getIds();
            attentionMaskIds[i] = encodings[i].getAttentionMask();
            tokenTypeIds[i] = encodings[i].getTypeIds();
            specialTokenMask[i] = encodings[i].getSpecialTokenMask();
        }

        OnnxTensor inputIdsTensor = OnnxTensor.createTensor(environment, inputIds);
        OnnxTensor attentionMaskTensor = OnnxTensor.createTensor(environment, attentionMaskIds);
        OnnxTensor tokenTypeIdsTensor = OnnxTensor.createTensor(environment, tokenTypeIds);

        Map<String, OnnxTensor> inputs = new HashMap<>();

        inputs.put("input_ids", inputIdsTensor);
        inputs.put("attention_mask", attentionMaskTensor);
        inputs.put("token_type_ids", tokenTypeIdsTensor);

        try (OrtSession.Result results = session.run(inputs)) {
            OnnxValue lastHiddenState = results.get(0);

            float[][][] tokenEmbeddings = (float[][][]) lastHiddenState.getValue();

            float[][] embeddingsVector = meanPooling(tokenEmbeddings, attentionMaskIds, specialTokenMask);

            long embeddingsEndTime = System.currentTimeMillis();

            System.out.println("Mode - Batch Encode");
            System.out.println("Sample size - " + sentences.size() + " sentences");
            System.out.printf("Time for loading tokenizer: %.2f seconds\n", (tokenizerEndTime - tokenizerStartTime) / 1000.0);
            System.out.printf("Time for loading model: %.2f seconds\n", (modelLoadEndTime - modelLoadStartTime) / 1000.0);
            System.out.printf("Time for generating tokens: %.2f seconds\n", (tokenGenerationEndTime - tokenGenerationStartTime) / 1000.0);
            System.out.printf("Time for generating embeddings: %.2f seconds\n", (embeddingsEndTime - embeddingsStartTime) / 1000.0);
            // Define the output CSV file path
            String outputFileName = "embeddings_1k_batch.csv";

            // Convert the embeddings to CSV format and write to file
            try {
                writeEmbeddingsToCSV(sentences, embeddingsVector, outputFileName, urls, useCSV);
                System.out.println("CSV file created successfully!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Clean up tensors
        inputIdsTensor.close();
        attentionMaskTensor.close();
        tokenTypeIdsTensor.close();

        // Close the session and environment
        session.close();
        environment.close();
    }

    public static float[][] meanPooling(float[][][] tokenEmbeddings, long[][] attentionMask, long[][] special_tokens_mask) {
        int numSentences = tokenEmbeddings.length;
        int numDimensions = tokenEmbeddings[0][0].length;
        float[][] mean = new float[numSentences][numDimensions];
        for (int i = 0; i < numSentences; i++) { // for each sentence
            int denominator = 0;
            for (int j = 0; j < tokenEmbeddings[i].length; j++) { // for each token
                // we need to make sure we ignore all added tokens like [CLS], [SEP], [PAD]
                if (attentionMask[i][j] == 1 && special_tokens_mask[i][j] == 0) {
                    denominator++;
                    for (int k = 0; k < numDimensions; k++) { // for each dimension
                        mean[i][k] += tokenEmbeddings[i][j][k];
                    }
                }
            }
            for (int k = 0; k < numDimensions; k++) { // for each dimension
                mean[i][k] /= denominator;
            }
        }
        return mean;
    }

    public static void writeEmbeddingsToCSV(List<String> sentences, float[][] embeddings, String csvFilePath, List<String> urls, boolean useCSV) throws IOException, NoSuchAlgorithmException {
        StringBuilder sb = new StringBuilder();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        for (int i = 0; i < embeddings.length; i++) {

            String embeddingString = Arrays.toString(embeddings[i]);
            if (useCSV) {
                String url = urls.get(i);
                sb.append(url).append(",");
            } else {
                String sentence = sentences.get(i);
                byte[] hash = digest.digest(sentence.getBytes());
                String hashKey = bytesToHex(hash);
                sb.append(hashKey).append(",");
            }

            sb.append("\"").append(embeddingString).append("\"");

            sb.append("\n");
        }
        try (FileWriter writer = new FileWriter(csvFilePath)) {
            writer.write(sb.toString());
        }
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : bytes) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
