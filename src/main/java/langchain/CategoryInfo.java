package langchain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CategoryInfo {
//    @JsonProperty("explanation")
    private String explanation;

//    @JsonProperty("contextCategory")
    private String contextCategory;

//    @JsonProperty("confidenceScore")
    private int confidenceScore;
}

