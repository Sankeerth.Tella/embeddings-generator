package utils;

import java.util.List;

public class CsvData {
    private List<String> keys;
    private List<String> documents;

    public CsvData(List<String> keys, List<String> documents) {
        this.keys = keys;
        this.documents = documents;
    }

    public List<String> getKeys() {
        return keys;
    }

    public List<String> getDocuments() {
        return documents;
    }
}