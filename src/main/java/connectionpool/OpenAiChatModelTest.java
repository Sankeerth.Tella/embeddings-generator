package connectionpool;

import dev.langchain4j.model.chat.ChatLanguageModel;
import dev.langchain4j.model.input.Prompt;
import dev.langchain4j.model.input.structured.StructuredPrompt;
import dev.langchain4j.model.input.structured.StructuredPromptProcessor;
import dev.langchain4j.model.openai.OpenAiChatModel;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class OpenAiChatModelTest {

    public static void main(String[] args) {
        // Create the pool configuration
        GenericObjectPoolConfig<ChatLanguageModel> config = new GenericObjectPoolConfig<>();
        config.setMaxTotal(10);  // Maximum number of connections
        config.setMinIdle(2);    // Minimum idle connections in the pool
        config.setMaxIdle(5);    // Maximum idle connections in the pool
        config.setTestOnBorrow(true); // Test the connection before borrowing
        config.setMaxWaitMillis(1000); // Wait up to 1 second for a connection

        // Create the pool
        String apiKey = "sk-proj-VqhPTIjw1UxzDNJuUEQLT3BlbkFJdImk5uimA8OiwEhaWAup";
        GenericObjectPool<ChatLanguageModel> pool = new GenericObjectPool<>(new ChatLanguageModelFactory(apiKey), config);

        // Create an ExecutorService with 2 threads
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        try {
            // Create tasks for the threads
            Callable<String> task1 = new OpenAiTask(pool);
            Callable<String> task2 = new OpenAiTask(pool);

            // Submit tasks and get futures
            Future<String> future1 = executorService.submit(task1);
            Future<String> future2 = executorService.submit(task2);

            // Retrieve and print results
            String response1 = future1.get();
            String response2 = future2.get();
            System.out.println("Response from thread 1: " + response1);
            System.out.println("Response from thread 2: " + response2);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Shut down the executor service and pool
            executorService.shutdown();
            pool.close();
        }
    }

    static class OpenAiTask implements Callable<String> {
        private final GenericObjectPool<ChatLanguageModel> pool;

        public OpenAiTask(GenericObjectPool<ChatLanguageModel> pool) {
            this.pool = pool;
        }

        @Override
        public String call() throws Exception {
            ChatLanguageModel model = null;
            try {
                // Borrow a connection from the pool
                model = pool.borrowObject();

                // Define your prompt
                CategorySuggestionPrompt categorySuggestionPrompt = new CategorySuggestionPrompt();
                categorySuggestionPrompt.text = "Debit & Checking Services, Checking account, Free checking account, Best checking account";
                categorySuggestionPrompt.catalogCategories = "Arts & Entertainment. Autos & Vehicles. Beauty & Cosmetology. Books & Literature. Business. Careers, Jobs, & Education. Computers & Electronics. Content Channel. Content Media Format. Content Source. Content Type. Economy. Finance. Food & Drink. Hobbies, Interests, Leisure & Games. Home & Garden. Industry. Internet & Telecom. Law, Government, News & Politics. Medical Health. Online Communities. People & Society. Personal Care. Personal Finance. Pets & Animals. Real Estate. Reference. Science. Shopping. Sports. Style & Fashion. Travel & Transportation. Consumer Advocacy & Protection. Coupons & Discount Offers. Customer Services. Identity Theft Protection. Product Reviews & Price Comparisons";
                Prompt prompt = StructuredPromptProcessor.toPrompt(categorySuggestionPrompt);

                // Perform the API call
                return model.generate(prompt.toUserMessage()).content().text();

            } finally {
                if (model != null) {
                    // Return the connection to the pool
                    pool.returnObject(model);
                }
            }
        }
    }

    static class ChatLanguageModelFactory extends BasePooledObjectFactory<ChatLanguageModel> {
        private final String apiKey;

        public ChatLanguageModelFactory(String apiKey) {
            this.apiKey = apiKey;
        }

        @Override
        public ChatLanguageModel create() throws Exception {
            return OpenAiChatModel.builder()
                    .apiKey(apiKey)
                    .modelName("gpt-4o")
                    .maxTokens(300)
                    .logRequests(true)
                    .logResponses(true)
                    .timeout(Duration.ofSeconds(60))
                    .build();
        }

        @Override
        public PooledObject<ChatLanguageModel> wrap(ChatLanguageModel model) {
            return new DefaultPooledObject<>(model);
        }
    }


    @StructuredPrompt(
            "You are an application whose job is to read an advertiser's description of web content and then rate " +
                    "how confident you are that the description of web content is contextually relevant to each of a set of web " +
                    "content categories for the purposes of advertising on that content. The different web content categories to " +
                    "consider are: {{catalogCategories}}.\n Confidence level is between 0 and 10. Only output results for categories " +
                    "which you rate higher than 3 Your response should be formatted using the following as an example:" +
                    "[{\"explanation\": \"short explanation for your rating\", \"contextCategory\": \"the name of the category\", \"confidenceScore\": (integer between 0 and 10)}]" +
                    "Do not include anything else in your response. The web content description is: {{text}}"
    )
    static class CategorySuggestionPrompt {
        private String text;
        private String catalogCategories;
    }

}
