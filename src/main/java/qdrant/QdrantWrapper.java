package qdrant;

import io.qdrant.client.PointIdFactory;
import io.qdrant.client.QdrantClient;
import io.qdrant.client.QdrantGrpcClient;
import io.qdrant.client.WithVectorsSelectorFactory;
import io.qdrant.client.grpc.JsonWithInt;
import io.qdrant.client.grpc.Points;
import io.qdrant.client.grpc.Points.ScoredPoint;
import io.qdrant.client.grpc.Points.SearchPoints;
import lombok.Getter;

import static io.qdrant.client.WithPayloadSelectorFactory.include;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.qdrant.client.WithPayloadSelectorFactory.enable;


public class QdrantWrapper {
    public static String collectionName = "universe_random_sample_100K@LATEST";
    public static String metaDataCollectionName = "universe_random_sample_100K_metadata@LATEST";
    public static QdrantClient client = new QdrantClient(
            QdrantGrpcClient.newBuilder("k8s-tcp-gw1.test.valassisdigital.net", 6334, false)
                    .withApiKey("puppies").build());

    public static void main(String[] args) throws Exception {
        List<Float> vector = Stream.of(-0.1111017, 0.46314922, 0.42108467, -0.009857451, 0.47186264, -0.18645528, 0.24428137, 0.3537716, 0.044790395, -0.3803496, 0.086499676, -0.25049266, -0.097030796, 0.56505954, 0.15079196, 0.65292925, 0.28533486, -0.0838676, -0.18620797, 0.49279216, 0.10675204, -0.07124163, 0.14733978, 0.9195923, 0.41530022, -0.055801608, -0.43463087, 0.076796494, -0.2773232, -0.014412044, 0.45203102, 0.12320057, -0.2474767, -0.62846196, 0.037317064, 0.15898421, -0.20545144, -0.58702207, 0.15216678, 0.17946644, -0.5939011, -0.28966194, 0.08219299, -0.079405166, -0.22037011, -0.2070438, 0.01676871, 0.2172902, -0.16601385, 0.10415645, -0.35268128, 0.14079188, 0.14687769, -0.1610563, 0.32909438, 0.62997144, -0.19035587, -0.2138015, -0.3955753, -0.31305724, -0.21207538, 0.24025169, 0.18302214, -0.6967644, -0.09971178, 0.22699146, 0.10964607, 0.25510374, -0.43845278, -0.10135566, -0.18930167, 0.12302475, -0.05662208, -0.18552281, -0.48956597, -0.027866306, -0.23253773, 0.08785879, -0.05024437, 0.13016437, -0.16919377, 0.48455068, 0.06844691, 0.5967233, 0.26635912, 0.17634048, 0.14185409, 0.5004525, -0.4503336, 0.3085543, -0.21936849, -0.27698573, 0.238863, 0.37478653, 0.31848148, -0.043305587, 0.23819223, -0.18431798, -0.5094141, 0.41764557, 0.22944778, -0.15716767, 0.29855937, 0.14947808, -0.10810251, 0.3275993, 0.10559065, 0.017458938, -0.13522092, 0.400136, 0.40171894, -0.21613833, -0.44685173, -0.22431308, 0.30283594, -0.29090345, 0.15363148, -0.013663782, -0.08971055, 0.0067829094, 0.24077854, -0.342498, 0.037543047, 0.8632068, -0.17395799, -0.2755598, -0.1819064, 0.08089357, -0.10820428, -0.2815944, 0.15003304, 0.34668222, -0.05301774, -0.40302932, -0.4398145, 0.55569315, 0.14605173, -0.3167896, -0.25752863, 0.36202532, -0.13134836, -0.507038, 0.5143334, -0.11413956, 0.1671541, -0.23770416, -0.005266519, -0.13937375, 0.33723637, -0.076934196, 0.35144418, -0.13648677, -0.25868085, -0.40636337, -0.3119706, -0.39427894, -0.7010993, 0.46943414, 0.34024408, -0.07544008, -0.05969494, -0.15273689, -0.105014116, 0.014805459, -0.32415545, -0.07597336, 0.030039497, 0.6924847, -0.56639886, 0.24829933, -0.051056433, 0.2286428, 0.7246574, 0.21091264, -0.008096882, 0.11062624, 0.3371747, 0.20765823, 0.016401099, 0.40769956, -0.8066095, 0.43792585, 0.061672766, -0.35706067, 0.0906462, -0.17233433, 0.20927395, -0.2852731, -0.027962856, -0.38843933, -0.22590168, -0.062021855, -0.28949, -0.16838986, 0.014297976, -0.054803442, -0.09577386, 0.10787015, -0.4432819, -0.14952211, 0.07487432, 0.097632386, -0.14539264, 0.026691964, 0.13832209, -0.7529035, -0.015157564, -0.39126775, -0.38215938, -0.22545937, -0.4622482, -0.037539866, 0.123355895, 0.3420415, -0.15398908, 0.010284569, -0.20956661, -0.14358269, -0.21542418, -0.28469527, -0.052953374, 0.14639154, -0.54039556, 0.24765724, -0.18546075, 0.7688221, -0.24188463, -0.71894497, 0.35324785, 0.70270765, 0.16011712, -0.29622862, 0.36576396, -0.3999448, -0.29551432, 0.030722866, -0.24174748, -0.24268487, 0.47848853, -0.4262415, -0.38837722, 0.15101199, 0.16341878, 0.26242116, 0.18664962, -0.21733575, -0.2249064, 0.04760271, -0.024553929, -0.22288336, -0.45767173, -0.14920099, -0.10447631, -0.11249311, 0.0054646535, -0.40000165, 0.26301497, -0.43151805, 0.016209379, 0.048345793, 0.4028582, -0.10666836, 0.13585912, -0.09753366, -0.33177602, -0.5278534, 0.16940705, 0.16995423, 0.41281578, -0.020707121, 0.17536393, 0.20945935, 0.31231868, 1.0209842, -0.3045088, 0.10747672, 0.23989862, -0.2123168, 0.16570304, -0.11626359, 0.16512339, 0.45734173, -0.48145264, -0.4622631, -0.4743422, -0.04236363, 0.4895515, -0.03481032, -0.07156334, -0.102866, 0.02009204, 0.18103144, -0.35559803, -0.1066583, 0.36742455, 0.053572915, 0.09673833, 0.15608078, -1.8650522E-4, -0.09285001, -0.20660931, -0.48645335, 0.20859462, 0.097771995, -0.13617459, 0.21814826, 0.16056925, -0.39716038, -3.3226817, 0.18450053, 0.3511257, -0.19613968, 0.11574946, 0.16154225, -0.17267807, -0.1013619, -0.25163272, 0.09839829, -0.07566118, -0.32151908, 0.18171187, 0.37814686, 0.29005826, -0.21998793, -0.13131002, -0.22849141, -0.29544517, 0.38803557, -0.1410777, -0.6939483, 0.42057618, -0.08773934, 0.33755466, 0.10629628, -0.36971346, -0.267607, -0.18007235, -0.26876718, -0.16228054, -0.34823218, -0.1019144, 0.29915112, 0.3108541, 0.19785331, 0.15367514, -0.5076611, -0.2363118, -0.6041149, -0.0033264346, -0.68091273, -0.10911648, -0.17348088, 0.88800246, -0.2442381, 0.24216083, -0.023445927, 0.11586218, 0.09585206, 0.18254027, 0.11758564, -0.1115407, -0.21041039, -0.06272361, -0.17444919, 0.6181681, 0.20914888, 0.16378804, -0.43154797, 0.4108815, -0.16605523, -0.52159953, 0.1360301, 0.067177415, -0.31225368, -0.84671646, -0.24256322, 0.049789153, 0.17927714, -0.14956774, 0.5678627, -0.4799842, -0.1816066, -0.18377677, -0.44623616, 0.33921692, -0.28018013, 0.037937768, -0.282165, -0.24415256, -0.4109149, 0.017744746, -0.12019471, -0.15677899, -0.4018617, -0.030191742, -0.113482185, -0.3100562, -0.4523424, 0.4050017, 0.37688157, -0.20509519, 0.14712039, 0.18551785, -0.109306306, 0.2913966, 0.289993, 0.07354934, -0.18274036, -0.012428385, -0.028332407, 0.49336517, -0.07726924, -0.05565811, -0.043544535, -0.20300552, 0.021676047, 0.28170615, -0.18919747, 0.035249155, 0.06932292, 0.29522476, -0.41844177, 0.205679, -0.17833617, 0.3186628, 0.7467182, 0.04362552, -0.197019, 0.2480697, 0.73756796, -0.0109694395, 0.47343895, -0.15540874, 0.24649593, -0.12325538, 0.3336663, -0.16989106, -0.35225084, -0.38304248, -0.52761245, -0.0092280265, 0.30126727, 0.35570952, 0.1852934, -0.10868537, -0.84960103, -0.10171918, 0.29430732, 0.38254467, 0.19005905, 0.17923921, -0.30159917, 0.028227014, 0.4406729, 0.052698985, 0.028106982, -0.28096575, 0.09709931, -0.6300743, -0.10859468, -0.17080885, -0.54601014, 0.32763165, 0.23742387, -0.07813807, -0.08246891, 0.22883631, -0.6418274, 0.29686737, 0.28004134, 0.37577742, 0.082727715, -0.48053503, 0.36018673, -0.15904944, -0.08336523, -0.1609815, -0.1820312, -0.044227377, -0.10910234, 0.3217761, -0.57863504, -0.14324185, 0.4444924, 0.013386595, -0.49154943, -0.100434884, 0.45297414, 0.050016318, -0.17455085, -0.1352737, -0.059071094, 0.49783957, 0.17366137, 0.23750019, -0.39148432, -0.09944933, -0.049295064, 0.046363566, 0.6422515, -0.05863871, -0.41043657, -0.52618176, -0.24567892, 0.2955194, -0.20437151, -0.060386788, 0.016278127, 0.47453552, -0.2717183, -0.17487156, 0.28636298, 0.096527554, -0.25666133, -0.047374774, 0.14332055, -0.24323787, 0.22629263, -0.4533747, -0.21109377, -0.03925769, 0.15630296, 0.4252147, -0.49061102, -0.15618429, -0.12500761, -0.27027687, 0.23654908, 0.020192968, -0.25148737, 0.14866114, 0.08986918, -0.4307689, -0.1030252, -0.040014714, 0.35627994, -0.2843008, -0.47282475, 0.10158012, -1.0678805, -0.06926345, -0.005181662, -0.19617118, 0.28089154, -0.16577345, -0.6861708, 0.081212215, -0.14881282, 0.085476235, -0.18738405, -0.28370383, -0.22238457, 0.21392442, 0.07730328, -0.08704064, 0.5797174, -0.399857, -0.025873132, -0.1623127, -0.015919024, -0.124704465, -0.4990872, 0.123740315, -0.56712323, -0.15746553, -0.122625485, -0.15808065, -0.20522587, -0.2161049, -0.5366658, -0.23663458, 0.058216356, 0.1660961, 0.040428486, 0.16572866, -0.09686419, -0.014457172, -0.5374958, 0.326191, -0.11081634, -0.1827023, -0.22757515, 0.06422282, 0.25003356, 0.22313665, -0.45884258, 0.28215367, -0.7298662, -0.021396728, -0.063576356, -0.24340573, -0.21523684, 0.23089594, -0.37935218, -0.19063666, 0.33904693, 0.1470143, -0.23609757, 0.48708075, -0.38086393, 0.16127843, 0.33939397, -0.087139755, 0.18368517, 0.8277112, 0.27419248, 0.21253455, -0.015452375, -0.113891736, 0.02560209, 0.0047369394, -0.24382697, 0.050411858, -0.15801597, 0.18764377, -0.46916336, -0.24176358, 0.19914797, -0.042390257, -0.29641563, 0.5992624, 0.47743198, -0.14472896, 0.044092644, 0.093610495, 0.049083326, 0.4955166, 0.117285006, -0.0028816245, -0.071054466, 0.27927923, 0.052098937, 0.5903465, 0.5855953, -0.19576994, -0.49436224, 0.04730743, 0.35644662, -0.1353666, 0.5571484, -0.34838742, 0.5294739, -0.14622289, -0.24491741, 0.017224122, 0.45475206, 0.19637883, 0.30949128, 0.06973835, 0.01392552, 0.38543856, 0.58469063, 0.035670303, 0.33342686, 0.4369327, 0.19683124, 0.22739008, -0.10425059, 0.33903474, 0.14740984, 0.2858, 0.034825597, 0.56217897, -0.3851937, 0.34128845, 0.34408876, 0.24268112, 0.9197615, 0.13760296, 0.3996529, 0.6389264, -0.75697565, -0.23128147, 0.06693471, 0.45359418, -0.6012467, 0.08873512, 0.072877854, 0.26419187, 0.24667658, -0.56242174, -0.32920507, 0.19442366, 0.3016502, -0.100479335, -0.32147732, -0.16810083, 0.27119377, 0.013906926, 0.0154846795, -0.42931437, -0.22385877, 0.006203799, -0.12259198, -0.1388067, -0.13645148, -0.18730332, -0.28905785, 0.24640921, 0.07352508, 0.2472628, -0.13804317, -0.17213096, -0.17618358, 0.3294254, 0.09384873, 0.39151785, -0.18015772, 0.19099513, -0.092990585, -0.5057629, 0.12817498, 0.11211869, -0.060426094, 0.008703863, 0.0595431, -0.0923297, 0.10705173, -0.1119321, 0.1449598, -0.32240036, -0.052157607, -0.07412673, -0.029254254, 0.012590993, 0.16167954, -0.18982664, -0.34367067, -0.109092906, -0.15004939, 0.061655145, -0.16000322, -0.44141284, 0.112904236, 0.23100823, 0.41208258, -0.01270232, 0.16009158, -0.13628012, 0.40917578, -0.29592988, 0.021216191, 0.1807093, -0.29422, -0.27856186, 0.06525677, -0.19247338, 0.021533322, 0.18376526, 0.488558, 0.10580743, -0.04515779, 0.582726, 0.028809264, -0.43346515, 0.03023273, -0.12693249, -0.20326653, -0.11905587, -0.090777285, 0.20706932, -0.20994265,
                -0.117047235, -0.5880291, 0.31079054, -0.03984377, 0.10974214, -0.231774).map(Double::floatValue).collect(Collectors.toList());
        List<ScoredPoint> searchResult;
//        searchResult = getTopNResults(1, vector);
//        System.out.println(searchResult.get(0));
//        searchResult = getMetaData();
//        System.out.println(searchResult);
//        ScoredPoint scoredPoint = searchResult.get(0);
//
//        CollectionMetadata collectionMetadata = new CollectionMetadata();
//        collectionMetadata.setCollectionName(scoredPoint.getPayloadMap().get("collectionName").getStringValue());
//        collectionMetadata.setUniverseSampleSize(scoredPoint.getPayloadMap().get("universeSampleSize").getIntegerValue());
//        collectionMetadata.setUniverseSampleRequests(scoredPoint.getPayloadMap().get("universeSampleRequests").getIntegerValue());
//        collectionMetadata.setUniverseActiveSize(scoredPoint.getPayloadMap().get("universeActiveSize").getIntegerValue());
//        collectionMetadata.setUniverseActiveRequests(scoredPoint.getPayloadMap().get("universeActiveRequests").getIntegerValue());
//        collectionMetadata.setCreationTimeMs(scoredPoint.getPayloadMap().get("creationTimeMs").getIntegerValue());
//        collectionMetadata.setCreationUser(scoredPoint.getPayloadMap().get("creationUser").getStringValue());
//        System.out.println(collectionMetadata.getCollectionName());

//        System.out.println(fetchByIds(Arrays.asList(98545L)).get(0).getPayloadMap());
//        List<Points.BatchResult> batchResult = spacedSearch(vector);
//        int batchSize = batchResult.size();
//        List<QdrantResult> results = new ArrayList<>();
//        for(int i=0; i<batchSize; i++) {
//            int currentBatch = i+1;
//            List<ScoredPoint> currentBatchList = batchResult.get(i).getResultList();
//            for(int j=0; j<currentBatchList.size(); j++){
//                ScoredPoint point = currentBatchList.get(j);
//                QdrantResult result = new QdrantResult();
//                result.setId(point.getId().getNum());
//                result.setScore(point.getScore());
//                Map<String, JsonWithInt.Value> payLoadMap = point.getPayloadMap();
//                result.setBatch(currentBatch);
//                result.setUrl(payLoadMap.get("url").getStringValue());
//                result.setRequests(payLoadMap.get("requests").getIntegerValue());
//                results.add(result);
//            }
//        }
//
//        List<QdrantResult> results_1 = IntStream.range(0, batchResult.size())
//                .boxed()
//                .flatMap(i -> {
//                    int currentBatch = i + 1;
//                    return batchResult.get(i).getResultList().stream()
//                            .map(point -> {
//                                QdrantResult result = new QdrantResult();
//                                result.setId(point.getId().getNum());
//                                result.setScore(point.getScore());
//                                result.setBatch(currentBatch);
//
//                                Map<String, JsonWithInt.Value> payloadMap = point.getPayloadMap();
//                                result.setUrl(payloadMap.get("url").getStringValue());
//                                result.setRequests(payloadMap.get("requests").getIntegerValue());
//
//                                return result;
//                            });
//                })
//                .collect(Collectors.toList());
//
//
//        System.out.println(batchResult);
        client.close();
    }

    public static List<ScoredPoint> getTopNResults(int limit, List<Float> vector) throws ExecutionException, InterruptedException {
        return client.searchAsync(
                SearchPoints.newBuilder()
                        .setCollectionName(collectionName)
                        .setLimit(limit)
                        .addAllVector(vector)
                        .setWithPayload(include(List.of("requests", "url")))
                        .build()).get();
    }

    public static List<ScoredPoint> getMetaData() throws ExecutionException, InterruptedException {
        return client.searchAsync(
                SearchPoints.newBuilder()
                        .setCollectionName(metaDataCollectionName)
                        .addAllVector(List.of(0.0f))
                        .setWithPayload(enable(true))
                        .setLimit(1)
                        .build())
                        .get();
    }

    public static List<Points.ScoredPoint> requestCounts(List<Float> searchVector, Float scoreThreshold, Integer topN) {
        if (topN == null) {
            topN = 50000;
        }
        if (scoreThreshold == null) {
            scoreThreshold = -1.0f;
        }
        try {
            return client.searchAsync(
                    SearchPoints.newBuilder()
                            .setCollectionName(collectionName)
                            .addAllVector(searchVector)
                            .setWithPayload(include(List.of("requests")))
                            .setScoreThreshold(scoreThreshold)
                            .setLimit(topN)
                            .build()).get();
        }
        catch (Exception e) {
            throw new RuntimeException("Error fetching requestCounts", e);
        }



    }


    public static List<Points.RetrievedPoint> fetchByIds(List<Long> ids) {
        try {
            // Convert List<Long> to List<PointId>
            List<Points.PointId> pointIds = ids.stream()
                    .map(PointIdFactory::id)
                    .collect(Collectors.toList());
            return client.retrieveAsync(collectionName, pointIds, true, false, null).get();
        }
        catch (Exception e) {
            throw new RuntimeException("Error fetching requestCounts", e);
        }
    }

    public static List<Points.BatchResult> spacedSearch(List<Float> searchVector) {
        float scoreThreshold = 0.50f;
        int urlsPerSample =2;
        int numSamples =2;
        int totalUrls =100000;
        List<Points.ScoredPoint> allResults = new ArrayList<>();
        int offset = 0;
        List<Points.SearchPoints> searchQueries = new ArrayList<>();
        while (offset < totalUrls) {
            searchQueries.add(Points.SearchPoints.newBuilder()
                    .setCollectionName(collectionName)
                    .addAllVector(searchVector)
                    .setLimit(urlsPerSample)
                    .setWithPayload(include(List.of("requests", "url")))
                    .setWithVectors(WithVectorsSelectorFactory.enable(true))
                    .setScoreThreshold(scoreThreshold)
                    .setOffset(offset)
                    .build());
            offset += totalUrls / numSamples;
        }
        try {
            return client.searchBatchAsync(collectionName, searchQueries, null).get();

        } catch(Exception e){
            throw new RuntimeException("Error while performing spacedSearch", e);
        }

    }
}
